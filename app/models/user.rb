class User < ApplicationRecord
  validates :name,:email,:password,presence: true
  before_create :expire_time_create
  def cron_job
    User.where(self.created_at+10.minutes=self.expire_time_user).destroy_all
  end
  private
  def expire_time_create
    self.expire_time_user=self.created_at.to_datetime+10.minutes
  end
end
