class ExpireTimeToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :expire_time_user, :timestamp
  end
end
